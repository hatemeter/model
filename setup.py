import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="hatemeter",
    version="0.1.0",
    author="Sergio Perez",
    author_email="sergiopr89@gmail.com",
    description="Logistic regression model for sentiment analysis",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hatemeter/model",
    packages=setuptools.find_packages(),
    package_data={"hatemeter": ["model.pkl"]},
    include_package_data=True,
    install_requires=[
        "nltk",
        "numpy",
        "pandas",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
