# Hatemeter ML model library
Based on Andrew's NLP notebook.  
This model uses a Logistic Regression model to train a very simple sentiment analysis algorithm.

## Requirements
You may need to install:  
* Python3
* Pip3
* Pipenv

An example of installation in Ubuntu:  
```bash
$ sudo apt install python3 python3-pip && \
sudo pip3 install --upgrade pip \
sudo pip3 install --upgrade pipenv
```

## Initialize the project the first time
In the project root folder:  
```bash
$ pipenv install --dev
```

## Run the main.py Jupyter Lab notebook
In the project root folder:
```bash
$ pipenv run jupyter lab
```  
It will open the notebook server in a browser tab, just open the file hatemeter/main.py "as Notebook" and try by yourself.
