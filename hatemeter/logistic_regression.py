import pickle
import nltk
import os.path
import numpy as np
import pandas as pd
from os import getcwd, getenv
from nltk.corpus import twitter_samples 
from hatemeter.utils import process_tweet, build_freqs, sigmoid, extract_features


class LogisticRegression(object):
    """ Logistic Regression model class """
    
    MODEL_FILE = 'model.pkl'
    NLTK_DATA_DIR = 'nltk_data/'
    
    def __init__(self):
        """ Constructor that initializes the right theta and accuracy values """
        self.theta = np.zeros((3, 1))
        self.model_accuracy = .0
        self.freqs = None
        self.get_nltk_data()
    
    def there_is_love(self, tweet):
        if self.love(tweet):
            print('Pure love <3')
        else:
            print('Pure hate >:(')
    
    def love(self, tweet):
        print(process_tweet(tweet))
        y_hat = self.predict(tweet)
        print(y_hat)
        if y_hat > 0.5:
            return True
        else: 
            return False
    
    def predict(self, tweet):
        '''
        Input: 
            tweet: a string
        Output: 
            y_pred: the probability of a tweet being positive or negative
        '''
        # extract the features of the tweet and store it into x
        x = extract_features(tweet, self.freqs)
        # make the prediction using x and theta
        y_pred = sigmoid(np.dot(x, self.theta))
        return y_pred
    
    def gradient_descent(self, x, y, alpha, num_iters):
        '''
        Input:
            x: matrix of features which is (m,n+1)
            y: corresponding labels of the input matrix x, dimensions (m,1)
            alpha: learning rate
            num_iters: number of iterations you want to train your model for
        Output:
            J: the final cost
        '''
        # get 'm', the number of rows in matrix x
        m = x.shape[0]
        for i in range(0, num_iters):
            # get z, the dot product of x and theta
            z = np.dot(x, self.theta)
            # get the sigmoid of z
            h = sigmoid(z)
            # calculate the cost function
            J = -1./m * (np.dot(y.transpose(), np.log(h)) + np.dot((1-y).transpose(),np.log(1-h)))    
            # update the weights theta
            self.theta = self.theta - (alpha/m) * np.dot(x.transpose(),(h-y))
        J = float(J)
        return J
    
    def get_nltk_data(self):
        # TODO: Directly distribute with the library to avoid each container downloading it
        # Load required data
        module_path = os.path.dirname(os.path.abspath(__file__))
        file_path = os.path.join(module_path, self.NLTK_DATA_DIR)
        nltk.data.path = [file_path]
        # NLTK should use envvar and data.path but instead ignores them so we need to enfoce in download_dir parameter
        nltk.download('twitter_samples', download_dir=file_path)
        nltk.download('stopwords', download_dir=file_path)
    
    def get_data(self):
        # select the set of positive and negative tweets
        all_positive_tweets = twitter_samples.strings('positive_tweets.json')
        all_negative_tweets = twitter_samples.strings('negative_tweets.json')
        # split the data into two pieces, one for training and one for testing (validation set) 
        test_pos = all_positive_tweets[4000:]
        train_pos = all_positive_tweets[:4000]
        test_neg = all_negative_tweets[4000:]
        train_neg = all_negative_tweets[:4000]
        # Merge positive and negative
        train_x = train_pos + train_neg 
        test_x = test_pos + test_neg
        # combine positive and negative labels
        train_y = np.append(np.ones((len(train_pos), 1)), np.zeros((len(train_neg), 1)), axis=0)
        test_y = np.append(np.ones((len(test_pos), 1)), np.zeros((len(test_neg), 1)), axis=0)
        return train_x, test_x, train_y, test_y
    
    def get_parameters(self, train_x, train_y):
        # collect the features 'x' and stack them into a matrix 'X'
        X = np.zeros((len(train_x), 3))
        for i in range(len(train_x)):
            X[i, :] = extract_features(train_x[i], self.freqs)
        # training labels corresponding to X
        Y = train_y
        # Apply gradient descent
        J = self.gradient_descent(X, Y, 1e-9, 1500)
        print(f"The cost after training is {J:.8f}.")
        print(f"The resulting vector of weights is {[round(t, 8) for t in np.squeeze(self.theta)]}")
    
    def train(self):
        # Get train and test data
        train_x, test_x, train_y, test_y = self.get_data()
        # create frequency dictionary
        self.freqs = build_freqs(train_x, train_y)
        # Execute training
        self.get_parameters(train_x, train_y)
        # Execute model test
        self.test_model(test_x, test_y)
        
    
    def test_model(self, test_x, test_y):
        """
        Input: 
            test_x: a list of tweets
            test_y: (m, 1) vector with the corresponding labels for the list of tweets
        """
        # the list for storing predictions
        y_hat = []
        for tweet in test_x:
            # get the label prediction for the tweet
            y_pred = self.predict(tweet)
            if y_pred > 0.5:
                # append 1.0 to the list
                y_hat.append(1)
            else:
                # append 0 to the list
                y_hat.append(0)
        # With the above implementation, y_hat is a list, but test_y is (m,1) array
        # convert both to one-dimensional arrays in order to compare them using the '==' operator
        self.accuracy = (y_hat==np.squeeze(test_y)).sum()/len(test_x)
        print(f"Logistic regression model's accuracy = {self.accuracy:.4f}")
    
    def load_model(self, path):
        with open(path, 'rb') as f:
            self.__dict__ = pickle.load(f)
    
    def load_module_model(self):
        module_path = os.path.dirname(os.path.abspath(__file__))
        model_file = os.path.join(module_path, self.MODEL_FILE)
        self.load_model(model_file)
    
    def save_model(self, path):
        with open(path, 'wb') as f:
            pickle.dump(self.__dict__, f)
